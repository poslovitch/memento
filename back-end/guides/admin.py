from django.contrib import admin
from .models import Guide

class GuideAdmin(admin.ModelAdmin):
    pass


admin.site.register(Guide, GuideAdmin)

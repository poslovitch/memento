from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('guides/', include('guides.urls')),
    path('oauth/', include('social_django.urls')),
]
